import winreg as wr
import os

def GetListStartUpProg(user = "current"):
    if(user == "current"):
        reg_key = wr.OpenKey(wr.HKEY_CURRENT_USER, r"Software\Microsoft\Windows\CurrentVersion\Run", 0, wr.KEY_READ)
    elif(user == "all"):
        reg_key = wr.OpenKey(wr.HKEY_LOCAL_MACHINE, r"Software\Microsoft\Windows\CurrentVersion\Run", 0, wr.KEY_READ)





    else:
        return 0
    print("\nПерелік:\n")
    try:
        count = 0
        while 1:
            name, value, type = wr.EnumValue(reg_key, count)
            name = repr(name)
            value = repr(value)
            print(f"Ключ: {name}; Значення: {value}")
            count = count + 1
        return 1
    except WindowsError:
        return 0
        pass
def AddProgToStartup(key, value):
    if(value[0] == '"'):
        value += " -autorun"
    else:
        value += " --autostart"
    try:
        reg_key = wr.OpenKey(wr.HKEY_CURRENT_USER, r"Software\Microsoft\Windows\CurrentVersion\Run", 0, wr.KEY_SET_VALUE)
        wr.SetValueEx(reg_key, key, 0, wr.REG_SZ, value)
        print(f"\nКлюч з назвою {key} і значенням {value} створено")
    except WindowsError:
        return 0
        pass
def GetListTaskShedule():
    dir1 = r"Software\Microsoft\Windows NT\CurrentVersion\Schedule\Taskcache\Tasks"
    reg_key = wr.OpenKey(wr.HKEY_LOCAL_MACHINE, dir1, 0, wr.KEY_READ)
    try:
        count1 = 0
        while 1:
            nameofsubkey = wr.EnumKey(reg_key, count1)
            dir2 = dir1 + "\\" + nameofsubkey
            reg_key2 = wr.OpenKey(wr.HKEY_LOCAL_MACHINE, dir2, 0, wr.KEY_READ)
            print(f"\nЗавдання: {nameofsubkey}")
            try:
                count2 = 0
                while 1:
                    name, value, type = wr.EnumValue(reg_key2, count2)
                    if(name == "Description"):
                        print(f"\n\tКлюч: {name}\n\t Значення: {value}")
                    elif(name == "Path"):
                        print(f"\tКлюч: {name}\n\t Значення: {value}")
                    count2 = count2 + 1
            except WindowsError:
                pass
            count1 = count1 + 1
        return 1
    except WindowsError:
        return 0
        pass
def SaveKey():
    reg_key = wr.OpenKey(wr.HKEY_CURRENT_USER, "SOFTWARE\Activision", 0, wr.KEY_ALL_ACCESS)
    if os.path.exists("C:\SavedKey.reg"):
        os.remove("C:\SavedKey.reg")
    try:
        wr.SaveKey(reg_key, "C:\SavedKey.reg")
    except WindowsError:
        print("Don't have right to save")
s = True;
while s:
    print("1.Список усіх програм та служб які завантажуються автоматично для усіх користувачів та поточного користувача")
    print("2.Список усіх програм та служб які завантажуються автоматично для поточного користувача")
    print("3.Додати програмну до автозавантаження програм для поточного користувача")
    print("4.Вивести список усіх завдань, які зареєстровані у планувальнику задач системи")
    print("5.Зберегти ключ HKEY_CURRENT_USER\\SOFTWARE")
    print("0.Вихід")
    i = input()
    if(i == '1'):
        GetListStartUpProg("all")
    elif(i == '2'):
        GetListStartUpProg()
    elif(i == '3'):
        print("\nВведіть назву програми: ")
        name = input()
        print("\nДиректорія файлу: ")
        direc = input()
        AddProgToStartup(name, direc)
    elif(i == '4'):
        GetListTaskShedule()
    elif(i == '5'):
        SaveKey()
    elif(i == '0'):
        s = False
    else:
        print("Введено некоректний символ")
    input()
    os.system('cls')
